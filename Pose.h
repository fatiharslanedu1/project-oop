/**
* @file Pose.h
* @Author Oguzhan Okur
* @date December, 2020
* \brief A Class for keeping coordiate values
*/


#ifndef POSE_H
#define POSE_H

#include <math.h>


    /*!
	
	*/
class Pose
{
friend class RobotControl;
private:
	float x;	
	float y;
	float th;
public:

	/*!
	 change x
	*/
	Pose();
	/*!
	* \return x
	*/
	float getX();
	/*!
	 change x
	*/
	void setX(float x);
	/*!
	* \return y
	*/
	float getY();
	/*!
	 change y
	*/
	void setY(float y);
	/*!
	* \return th
	*/
	float getTh();
	/*!
	 change th
	*/
	void setTh(float th);
	/*!
	 perator overloading ==
	*/
	bool operator==(const Pose& pos);
	/*!
	 operator overloading +
	*/
	Pose operator+(const Pose& pos);
	/*!
	 operator overloading -
	*/
	Pose operator-(const Pose& pos);
	/*!
	 operator overloading +=
	*/
	Pose& operator+=(const Pose& pos);
	/*!
	 operator overloading -=
	*/
	Pose& operator-=(const Pose& pos);
	/*!
	 operator overloading <
	*/
	bool operator<(const Pose& pos);
	/*!
	* \return Pose
	*/
	Pose& getPose();
	/*!
	 change Pose
	*/
	void setPose(float _x, float _y, float _th);
	/*!
	 finding distance to pos point (hypotenuse)
	*/
	float findDistanceTo(Pose pos);
	/*!
	 finding angle to pos point (degree)
	*/
	float findAngleTo(Pose pos);
	/*!
	 destructor
	*/
	~Pose();
};


#endif // !POSE_H