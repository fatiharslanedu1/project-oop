#pragma once
/**
 * @file RobotInterface.h
 * @Author Fatih Arslan
 * @date January, 2021
 * @brief Robot Operator functions in class
 */

#include<iostream>
#include "Pose.h"

class RobotInterface
{
	friend class PioneerRobotInterface;
private:
	Pose* position;
	int state;
public:
	/*!
	 Constructor
	*/
	RobotInterface();
	/*!
	 Turn left Function
	*/
	virtual void turnLeft() = 0;

	/*!
	 Turn right Function
	*/
	virtual void turnRight() = 0;

	/*!
	 Forward Function
	speed to contunie with given speed
	*/
	virtual void forward(float speed) = 0;

	/*!
	 Print to position of robot
	*/
	virtual void print() = 0;

	/*!
	 Backward function
	 speed to go back with given speed.
	*/
	virtual void backward(float speed) = 0;

	/*!
	brief Get function of pose
	return position from Pose class
	*/
	virtual Pose getPose() = 0;

	/*!
	 Set function of pose to PioneerRobotAPI from Pose.
	 p taken from Pose class
	*/
	virtual void setPose(Pose) = 0;

	/*!
	 function to stop turning.
	*/
	virtual void stopTurn() = 0;

	/*!
	 function to stop moving.
	*/
	virtual void stopMove() = 0;
	/*!
	 function to stop moving.
	*/
	virtual void updateSensors() = 0;

	/*!
	 Destructor
	*/
	~RobotInterface();
};

