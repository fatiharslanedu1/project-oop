//!! ControlPanel.cpp
#include "ControlPanel.h"

ControlPanel::ControlPanel():robot(&pioneerRobot, &laser, &sonar, &robotOperator)
{

}

ControlPanel::~ControlPanel()
{
}

void ControlPanel::menu()
{
	robotOperator.setAccessState(false);
	robotOperator.setUser("admin", "here", 1011);

	do {
		cout << "		  " << endl;
		cout << "Main Menu" << endl;
		cout << "1. Connection" << endl;
		cout << "2. Motion" << endl;
		cout << "3. Sensor" << endl;
		cout << "4. Path" << endl;
		cout << "5. Quit" << endl;
		cout << "Choose one : ";
		cin >> choice;

		switch (choice)
		{
		//! Case of connection.
		case 1:  
			nloop = 0;
			do {
				cout << "				 " << endl;
				cout << "Connection Menu " << endl;
				cout << "1. Connect Robot " << endl;
				cout << "2. Disconnect Robot " << endl;
				cout << "3. Open Access to Robot " << endl;
				cout << "4. Close Access to Robot " << endl;
				cout << "5. Back " << endl;
				cout << "Choose one : ";
				cin >> choice;

				switch (choice)
				{
				//!  Connecting to robot.
				case 1: 
					cout << "Please Enter a name for the output file: ";
					cin >> tmpName;
					record.setFileName(tmpName + ".txt");
					record.openFile();

					if (!pioneerRobot.connect())
					{
						cout << endl << "Robot could not connect....." << endl;
					}
					else
					{
						record.writeLine("Robot is connected.");
						cout << endl << "Robot is connected." << endl;
					}
					break;

				case 2:
				//! Disconnecting to Robot
					if (!pioneerRobot.disconnect())
					{
						cout << endl << "Robot could not disconnect....." << endl;
					}
					else
					{
						record.writeLine("Robot is disconnected.");
						cout << endl << "Robot is disconnected." << endl;
					}
					break;

				case 3:
				//!Open Access for Robot
					cout << "Please enter the access code : ";
					cin >> tmpPass;
					if (robot.openAccess(tmpPass))
					{
						cout << endl << "Robot access opened successfully." << endl;
						robotOperator.print();
						record.writeLine("Robot access opened.");
					}
					else
					{
						cout << endl << "Wrong access code. Failed." << endl;
					}
					break;

				case 4:
				//!Close Access To Robot
					cout << "Please enter the Access Code: ";
					cin >> tmpPass;
					if (robot.closeAccess(tmpPass))
					{
						cout << endl << "Robot access closed successfully." << endl;
						robotOperator.print();
						record.writeLine("Robot access closed.");
					}
					else
					{
						cout << endl << "Wrong access code. Failed for closing." << endl;
					}
					break;

				case 5: 
					nloop = 1;
					break;

				default:
					break;
				}

			} while (nloop == 0);
			break;

		case 2:
		//!Motion
			nloop = 0;
			do {
				cout << "			" << endl;
				cout << "Motion Menu" << endl;
				cout << "1. Move Robot" << endl;
				cout << "2. Safe Move Robot" << endl;
				cout << "3. Stop Moving" << endl;
				cout << "4. Turn Left" << endl;
				cout << "5. Turn Right" << endl;
				cout << "6. Forward" << endl;
				cout << "7. Move Distance" << endl;
				cout << "8. Back" << endl;
				cout << "Choose one : ";
				cin >> choice;

				switch (choice)
				{
				case 1:
				//! Move Robot
					if (robotOperator.checkAccessState())
					{
						cout << "Please Enter the Speed of Robot : ";
						cin >> tmpMoveSpeed;
						robot.forward(tmpMoveSpeed);
						record.writeLine("Robot is moving with speed of " + to_string(tmpMoveSpeed));
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}				
					break;

				case 2:
				//! Safe Move
					if (robotOperator.checkAccessState())
					{
						pioneerRobot.getSonarRanges(updatedSonar);
						sonar.updateSensor(updatedSonar);
						cin >> tmpMoveSpeed;
						while (sonar.getRange(4) > 650 && sonar.getRange(3) > 650) {
							robot.forward(tmpMoveSpeed);
							pioneerRobot.getSonarRanges(updatedSonar);
							sonar.updateSensor(updatedSonar);
						}
						robot.stopMove();

						updatedPose = robot.getPose();
						robot.addToPath();

						record.writeLine("Robot's position (X,Y,Th) : " + to_string(updatedPose.getX()) + "," + to_string(updatedPose.getY()) + "," + to_string(updatedPose.getTh()));
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}				
					break;
				case 3:
				//! Stop procedure.
					if (robotOperator.checkAccessState())
					{
						robot.stopMove();
						updatedPose = robot.getPose();
						record.writeLine("Robot has stopped moving.");
						record.writeLine("Robot's position (X,Y,Th) : " + to_string(updatedPose.getX()) + "," + to_string(updatedPose.getY()) + "," + to_string(updatedPose.getTh()));
						robot.addToPath();
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;

				case 4:
				//! Left operations.
					if (robotOperator.checkAccessState())
					{
						robot.turnLeft();
						updatedPose = robot.getPose();
						robot.addToPath();
						record.writeLine("Robot is turning to left.");
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;

				case 5:
				//! Turn Right. 
					if (robotOperator.checkAccessState())
					{
						robot.turnRight();
						updatedPose = robot.getPose();
						robot.addToPath();
						record.writeLine("Robot is turning to right.");
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;

				case 6:
				//!Forward
					if (robotOperator.checkAccessState())
					{
						cout << "Please enter the speed of robot : ";
						cin >> tmpMoveSpeed;
						robot.forward(tmpMoveSpeed);
						record.writeLine("Robot is moving forward with speed of " + to_string(tmpMoveSpeed));
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;

				case 7:
					//! Distance operations.
					if (robotOperator.checkAccessState())
					{
						updatedPose = robot.getPose();
						tmpPose = updatedPose;

						cout << "Please enter distance to go : ";
						cin >> tmpDistance;

						cout << "Please enter the speed of robot : ";
						cin >> tmpMoveSpeed;

						while (updatedPose.findDistanceTo(tmpPose) <= tmpDistance)
						{
							robot.forward(tmpMoveSpeed);
							updatedPose = robot.getPose();
						}
						robot.stopMove();

						tmpX = tmpPose.getX();
						tmpY = tmpPose.getY();
						tmpTh = tmpPose.getTh();

						record.writeLine("Robot's first position (X,Y,Th) : " + to_string(tmpX) + "," + to_string(tmpY) + "," + to_string(tmpTh));
						record.writeLine("Robot has moved distance of " + to_string(tmpDistance) + " with speed of " + to_string(tmpMoveSpeed));

						tmpX = updatedPose.getX();
						tmpY = updatedPose.getY();
						tmpTh = updatedPose.getTh();
						record.writeLine("Robot's last position (X,Y,Th) : " + to_string(tmpX) + "," + to_string(tmpY) + "," + to_string(tmpTh));

						robot.addToPath();
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}			
					break;

				case 8: 
					nloop = 1;
					break;

				default:
	
					break;
				}
			} while (nloop == 0);
			break;

		case 3: 

			nloop = 0;
			do {
				cout << "	" << endl;
				cout << "1. Get range from a selected laser sensor" << endl;
				cout << "2. Get minimum range from laser sensor	" << endl;
				cout << "3. Get maximum range from laser sensor	" << endl;
				cout << "4. Get angle of a selected laser sensor	" << endl;
				cout << "5. Get range from a selected sonar sensor" << endl;
				cout << "6. Get minimum range from sonar sensor	" << endl;
				cout << "7. Get maximum range from sonar sensor	" << endl;
				cout << "8. Get angle of a selected sonar sensor	" << endl;
				cout << "9. Update laser sensor" << endl;
				cout << "10. Update sonar sensor" << endl;
				cout << "11. Back" << endl;
				cout << "Choose one : ";
				cin >> choice;
				pioneerRobot.getSonarRanges(updatedSonar);
				sonar.updateSensor(updatedSonar);
				pioneerRobot.getLaserRanges(updatedLaser);
				laser.updateSensor(updatedLaser);
				switch (choice)
				{
				case 1:
					if (robotOperator.checkAccessState())
					{
						cout << "Enter a integer[0-180] to find range of laser sensor -> ";
						cin >> selected;
						if ((selected >= 0) && (selected <= 180))
						{
							cout << "-> " << laser.getRange(selected) << endl;
						}
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}				
					break;
				case 2:
					if (robotOperator.checkAccessState())
					{
						laser.getMin(a);
						cout << "Minimum range of laser sensor " << a << "range " << laser.getMin(a) << endl;
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 3:
					if (robotOperator.checkAccessState())
					{
						laser.getMax(a);
						cout << "Maximum range of laser sensor " << a << "range " << laser.getMax(a) << endl;
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 4:
					if (robotOperator.checkAccessState())
					{
						cout << "Enter a integer[0-180] to find angle of laser sensor -> ";
						cin >> selected;
						if ((selected >= 0) && (selected <= 180))
						{
							cout << "-> " << laser.getAngle(selected) << endl;
						}
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 5:
					if (robotOperator.checkAccessState())
					{
						cout << "Enter a integer[0-15] to find range of sonar sensor -> ";
						cin >> selected;
						if ((selected >= 0) && (selected <= 15))
						{
							cout << "-> " << sonar.getRange(selected) << endl;
						}
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 6:
					if (robotOperator.checkAccessState())
					{
						sonar.getMin(a);
						cout << "Minimum range of sonar sensor " << a << "range " << sonar.getMin(a) << endl;
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 7:
					if (robotOperator.checkAccessState())
					{
						int a;
						sonar.getMax(a);
						cout << "Maximum range of sonar sensor " << a << "range " << sonar.getMax(a) << endl;
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 8:
					if (robotOperator.checkAccessState())
					{
						cout << "Enter a integer[0-15] to find angle of sonar sensor -> ";
						cin >> selected;
						if ((selected >= 0) && (selected <= 15))
						{
							cout << "-> " << sonar.getAngle(selected) << endl;
						}
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 9:
					if (robotOperator.checkAccessState())
					{
						pioneerRobot.getLaserRanges(updatedLaser);
						laser.updateSensor(updatedLaser);

						record.writeLine("Laser sensor updated");
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 10:
					if (robotOperator.checkAccessState())
					{
						pioneerRobot.getSonarRanges(updatedSonar);
						sonar.updateSensor(updatedSonar);

						record.writeLine("Sonar sensor updated");
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 11:
					nloop = 1;
					break;
				default:
					break;
				}


			} while (nloop == 0);
			break;

		case 4:  
			nloop = 0;
			do {
				cout << "	" << endl;
				cout << "1. Add updated pose to path" << endl;
				cout << "2. Clear path" << endl;
				cout << "3. Record path to file" << endl;
				cout << "4. Back" << endl;
				cout << "Choose one : ";
				cin >> choice;
				switch (choice) {
				case 1: 
					if (robotOperator.checkAccessState())
					{
						robot.addToPath();
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 2:
					if (robotOperator.checkAccessState())
					{
						robot.clearPath();
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 3: 
					if (robotOperator.checkAccessState())
					{
						robot.recordPathToFile();
					}
					else
					{
						cout << endl << "You have no access to robot.Please open access from 'Connection Menu'." << endl;
					}
					break;
				case 4: 
					nloop = 1;
					break;
				default:
					break;
				}
			} while (nloop == 0);
			break;
		case 5:  
			loop = 1;
			break;

		default:
			break;
		}
	} while (loop == 0);
}