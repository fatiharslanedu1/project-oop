/**
 * @file RobotOperator.h
 * @Author Mahmut Kilic
 * @date December, 2020
 * @brief encryption and decryption operations on given value.
 */

#include "Encryption.h"
#include <iostream>
#include <math.h>
#include <string>

using namespace std;

#ifndef ROBOTOPERATOR_H
#define ROBOTOPERATOR_H
/*!
	encryption and decryption operations on given value.
*/
class RobotOperator
{
	Encryption tmp;
	string tmpState;
	//! users info
	string name;
	string surname;
	unsigned int accessCode;
	int encryptCode(int);
	int decryptCode(int);
	bool accessState;
public:

	//! if its the same with accessCode, given value
	bool checkAccessCode(int a);

	//! print argument.
	void print();	

	//! set Access operations for robot.
	void setAccessState(bool);

	//! true or false check.
	bool checkAccessState();

	//! set User
	void setUser(string, string, int);

};

#endif