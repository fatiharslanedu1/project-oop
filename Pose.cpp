
#include "Pose.h"	

Pose::Pose()
{
	this->x = 0;
	this->y = 0;
	this->th = 0;
}
float Pose::getX()
{
	return this->x;
}
void Pose::setX(float x)
{
	this->x = x;
}
float Pose::getY()
{
	return this->y;
}
void Pose::setY(float y)
{
	this->y = y;
}
float Pose::getTh()
{
	return this->th;
}
void Pose::setTh(float th)
{
	this->th = th;
}
bool Pose::operator==(const Pose & pos)
{
	if (this->getX() != pos.x)
		return false;
	if (this->getY() != pos.y)
		return false;
	if (this->getTh() != pos.th)
		return false;
	return true;
}
Pose Pose::operator+(const Pose& pos)
{
	Pose result;
	result.x = this->x + pos.x;
	result.y = this->y + pos.y;
	result.th = this->th + pos.th;

	return result;
}
Pose Pose::operator-(const Pose& pos)
{
	Pose result;
	result.x = this->x - pos.x;
	result.y = this->y - pos.y;
	result.th = this->th - pos.th;

	return result;
}
Pose& Pose::operator+=(const Pose& pos)
{
	this->x += pos.x;
	this->y += pos.y;
	this->th += pos.th;

	return *this;
}
Pose& Pose::operator-=(const Pose& pos)
{
	this->x -= pos.x;
	this->y -= pos.y;
	this->th -= pos.th;

	return *this;
}
bool Pose::operator<(const Pose& pos)
{
	if (this->getX() <= pos.x || this->getX() > pos.x)
		return false;
	if (this->getY() <= pos.y || this->getY() > pos.y)
		return false;
	if (this->getTh() <= pos.th || this->getTh() > pos.th)
		return false;
	return true;
}
Pose& Pose::getPose()
{
	return *this;
}
void Pose::setPose(float _x, float _y, float _th)
{
	x = _x;
	y = _y;
	th = _th;
}
float Pose::findDistanceTo(Pose pos)
{
	float a, b, c;
	a = this->x - pos.x;
	b = this->y - pos.y;
	c = (a*a) + (b*b);
	return sqrt(c);
}
float Pose::findAngleTo(Pose pos)
{
	float a;
	a = this->th - pos.th;
	if (a < 0)
		a *= -1;
	return a;
}
Pose::~Pose()
{
}