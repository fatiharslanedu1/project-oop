//** Record.cpp

#include "Record.h"
//! function which is open the file.
bool Record::openFile()
{
	file.open(fileName, ios::out | ios::in | ios::trunc);
	if (file.is_open())
	{
		return true;
	}
	else
	{
		return false;
	}
}
//! function which is close the file.
bool Record::closeFile()
{
	file.close();
	if (file.is_open())
	{
		return false;
	}
	else
	{
		return true;
	}
}
//! function which is set file name.
void Record::setFileName(string name)
{
	fileName = name;
}
int i = 0;
//! function which is read line by line.
string Record::readLine()
{	
	string str;
	file.seekg(i, ios::beg);
	getline(file, str);
	int x = str.length(); // get length
	i = i + x + 2;  // skip lines.
	return str;
}
//! function for writes lines.
bool Record::writeLine(string str)
{
	file.seekg(0,ios_base::end);
	file << str << endl;
	return true;
}
//! operator overload >> for input
istream& operator >> (istream& in, Record& a)
{
	in >> a;
	return in;
}
//! overload << for output.
ostream &operator<<(ostream& out, const Record& a)
{
	out << a;
	return out;
}

