/**
 * @file RobotOperator.h
 * @Author Oguzhan Okur
 * @date December, 2020
 * @brief get Pose class
 */

#include "Pose.h"
/*!
	Konumu tutan bir dugum gorevi gorur. Listeyi olusturmak icin kullanilir.
*/
#ifndef NODE_H_
#define NODE_H_
class Node {
public:
	Node* next;
	Pose pose;
};

#endif 
