#include "LaserSensor.h"

LaserSensor::LaserSensor()
{
	for (int i = 0; i < 181; i++)
	{
		ranges[i] = 0;
	}
	setAngles();
}

LaserSensor::LaserSensor(float nRanges[])
{
	updateSensor(nRanges);
	setAngles();
}

void LaserSensor::setAngles()
{
	for (int i = 0; i < 181; i++) 
	{
		angles[i] = 90-i;
	}
}

float LaserSensor::getMin(int& index)const
{
	int min = ranges[0];
	for (int i = 0; i < 181; i++)
	{
		if (min > ranges[i])
		{
			min = ranges[i];
			index = i;
		}
	}
	return min;
}

float LaserSensor::getMax(int& index)const
{
	int max = ranges[0];
	for (int i = 0; i < 181; i++)
	{
		if (max < ranges[i])
		{
			max = ranges[i];
			index = i;
		}
	}
	return max;
}
void LaserSensor::updateSensor(float nRanges[])
{
	for (int i = 0; i < 181; i++)
	{
		ranges[i] = nRanges[i];
	}
}

// Edit by Fatih Arslan
float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle)const
{
	int startAngleNo, endAngleNo,minRange,index=0,i;
	startAngleNo = 90 - startAngle;
	endAngleNo = 90 - endAngle;
	minRange = ranges[startAngleNo];
	if (startAngleNo < endAngleNo)
	{
		for (i = startAngleNo; i <= endAngleNo;i++)
		{
			if (ranges[i] < minRange)
			{
				minRange = ranges[i];
				index = i;
			}
		}
	}
	else if (startAngleNo > endAngleNo)
	{
		for (i = endAngleNo; i <= startAngleNo; i++)
		{
			if (ranges[i] < minRange)
			{
				minRange = ranges[i];
				index = i;
			}
		}
	}
	else
	{
		index = startAngleNo;
	}
	angle = 90 + index;
	return minRange;
}

float LaserSensor::getRange(int index) const
{
	return ranges[index];
}

float LaserSensor::getAngle(int index) const
{
	return angles[index];
}

float LaserSensor::operator[](int index)const
{
return getRange(index);
}