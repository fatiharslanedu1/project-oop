/**
 * @file RobotControl.h
 * @Author Fatih Arslan
 * @date  January, 2021
 * @brief definiton of robot control operations
 *
 */
#include<iostream>
#include<math.h>
#include "RobotOperator.h"
#include "Record.h"
#include "Path.h"
#include "RobotInterface.h"
#include "RangeSensor.h"
using namespace std;

#ifndef ROBOT_CONTROL_H
#define ROBOT_CONTROL_H

class RobotControl {

private:
	int state;
	RobotOperator *robotOperator;
	Record record;
	Path path;
	RangeSensor *sensor1;
	RangeSensor *sensor2;
	RobotInterface *robotInterface;
public:
	//! A constructor definition
	RobotControl(RobotInterface*, RangeSensor*, RangeSensor*, RobotOperator*);

	//! Turn left Function
	void turnLeft();

	//! Turn left Function
	void turnRight();
	
	//! Get function of pose
	//! return position from Pose class
	Pose getPose();

	/*!
	speed variables refers to contunie with given speed
	*/
	//! Forward Function
	void forward(float speed);

	//! print to position of robot
	void print();

	//! Backward function
	//! speed variables refers to go back with given speed
	void backward(float speed);

	
	//! Set function of pose to PioneerRobotAPI from Pose
	//! p taken from Pose class
	void setPose(Pose);

	//! function to stop Turning
	void stopTurn();

	//! function to stop Moving
	void stopMove();

	//! function to open access to robot if the access code is true.
	bool openAccess(int);

	/*!
	function add objects to path.
	*/
	bool addToPath();
	
	// Destructor function
	~RobotControl();

	//! function to close access to robot if the access code is true.
	bool closeAccess(int);
	
	/*!
	function clear path objects.
	*/
	bool clearPath();
	/*!
	function to write path to output file.
	*/
	bool recordPathToFile();
	
};

#endif	
