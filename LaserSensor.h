/**
* @file LaserSensor.h
* @Author Tolga Hardal
* @date December, 2020
* \brief Provides data retention and management for laser distance sensor.
*/


#include "PioneerRobotAPI.h"
#include "RangeSensor.h"

#ifndef LASERSENSOR_H_
#define LASERSENSOR_H_

class LaserSensor : public RangeSensor
{
private:
	/*!
	 distance information of the sensor
	*/
	float ranges[181];
	/*!
	 angle information of the sensor
	*/
	float angles[181];
	/*!
	 sets sensor default values
	*/
	void setAngles();
	PioneerRobotAPI* robotAPI;
public:
	/*!
	constitutive function of the sensors.
	*/
	LaserSensor();
	/*!
	 constituent function of the sensors.
	*/
	LaserSensor(float[]);
	
	float getRange(int)const;
	float operator[](int)const;
	float getAngle(int)const;

    /*!
	getMax returns the maximum of the distance values.
	*/
	float getMax(int&)const;
	/*!
	 getMin returns the minimum of the distance values.
	*/
	float getMin(int&)const;

	/*!
	  updateSensor uploads the current sensor distance values of the robot to the ranges array.
	*/
	void updateSensor(float[]);
	/*!
	 getClosestRange returns the smallest distance between two angles on angle, returns the distance with return.
	*/
	float getClosestRange(float, float, float&)const;
};
#endif 


