/**
 * @file PioneerRobotInterface.h
 * @Author Fatih Arslan
 * @date December, 2020
 * @brief Robot interface operations.
 */

#pragma once
#include<iostream>
#include"RobotInterface.h"
#include"PioneerRobotAPI.h"

using namespace std;

class PioneerRobotInterface : public RobotInterface {

private:

	/*!
	 Definition the object
	*/
	PioneerRobotAPI* robotAPI;

public:
	
	/*!
	 Constructor
	*/
	PioneerRobotInterface();

	/*!
	 Turn left Function
	*/
	void turnLeft();

	/*!
	 Turn right Function
	*/
	void turnRight();

	/*!
	 Forward Function
	speed to contunie with given speed
	*/
	void forward(float speed);

	/*!
	 Print to position of robot
	*/
	void print();

	/*!
	Backward function
	speed to go back with given speed
	*/
	void backward(float speed);

	/*!
	Get function of pose
	return position from Pose class
	*/
	Pose getPose();

	/*!
	 Set function of pose to PioneerRobotAPI from Pose
	 p taken from Pose class
	*/
	void setPose(Pose);

	/*!
	 function to stop turning
	*/
	void stopTurn();

	/*!
	function to stop moving
	*/
	void stopMove();

	/*!
	 update sensors function
	*/
	void updateSensors();

	/*!
	 	Destructor
	*/
	~PioneerRobotInterface();

	bool connect();
	bool disconnect();

	void getLaserRanges(float[]);
	void getSonarRanges(float[]);

};
