//!! RobotOperator.cpp

#include "RobotOperator.h"

int RobotOperator::encryptCode(int code)
{
	return tmp.encrypt(code);
}

int RobotOperator::decryptCode(int code)
{
	return tmp.decrypt(code);
}

bool RobotOperator::checkAccessCode(int code)
{
	if (tmp.encrypt(code) == accessCode)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void RobotOperator::print()
{
	if (accessState == true)
	{
		tmpState = "True";
	}
	else
	{
		tmpState = "False";
	}

	cout << "Name and Surname : " << name << " " << surname << endl;
	cout << "Access State of User : " << tmpState << endl;
}

void RobotOperator::setAccessState(bool check)
{
	if (check == true)
	{
		accessState = true;
	}
	if (check == false)
	{
		accessState = false;
	}
}
bool RobotOperator::checkAccessState()
{
	return accessState;
}
void RobotOperator::setUser(string _name, string _surname, int code)
{
	name = _name;
	surname = _surname;
	accessCode = tmp.encrypt(code);
}
