#include "Encryption.h"

int Encryption::encrypt(int a)
{             
	int b = a % 1000;           
	int ax = (a - b) / 1000;  
	int c = b % 100;          
	int bx = (b - c) / 100;  
	int d = c % 10;        
	int cx = (c - d) / 10; 

	ax = (ax + 7) % 10;
	bx = (bx + 7) % 10;
	cx = (cx + 7) % 10;
	d = (d + 7) % 10;

	int res = ax * 1000 + bx * 100 + cx * 10 + d;

	return res;
}
int Encryption::decrypt(int a)
{
	int b = a % 1000;
	int ax = (a - b) / 1000;
	int c = b % 100;
	int bx = (b - c) / 100;
	int d = c % 10;
	int cx = (c - d) / 10;

	ax = (ax + 3) % 10;
	bx = (bx + 3) % 10;
	cx = (cx + 3) % 10;
	d = (d + 3) % 10;

	int res = ax * 1000 + bx * 100 + cx * 10 + d;

	return res;
}