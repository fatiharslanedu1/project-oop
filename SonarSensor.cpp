#include "SonarSensor.h"

SonarSensor::SonarSensor(float nRanges[])
{
	updateSensor(nRanges);
	setAngles();
	
}
SonarSensor::SonarSensor() {
	for (int i = 0; i < 16; i++) {
		ranges[i] = 0;
	}
	setAngles();
}

void SonarSensor::setAngles() {
	angles[0] = -90;
	angles[1] = -50;
	angles[2] = -30;
	angles[3] = -10;
	angles[4] = 10;
	angles[5] = 30;
	angles[6] = 50;
	angles[7] = 90;
	angles[8] = 90;
	angles[9] = 130;
	angles[10] = 150;
	angles[11] = 170;
	angles[12] = -170;
	angles[13] = -150;
	angles[14] = -130;
	angles[15] = -90;
}



float SonarSensor::getMin(int& index)const
{
	int min = ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (min > ranges[i])
		{
			min = ranges[i];
			index = i;
		}
	}
	return min;
}
/// Edit by Fatih Arslan
float SonarSensor::getMax(int& index)const
{
	int max = ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (max < ranges[i])
		{
			max = ranges[i];
			index = i;
		}
	}
	return max;
}
/// Edit by Oguzhan Okur
void SonarSensor::updateSensor(float nRanges[])
{
	for (int i = 0; i < 16; i++)
	{
		ranges[i] = nRanges[i];
	}
}

float SonarSensor::getRange(int index) const
{
	return ranges[index];
}

float SonarSensor::getAngle(int index) const
{
	return angles[index];
}

float SonarSensor::operator[](int index)const
{
	return getRange(index);
}

float SonarSensor::getClosestRange(float a, float b, float &c)const
{
	int i;
	float sonuc = getMin(i);
	c = i;
	return sonuc;
}