/**
* @file RangeSensor.h
* @Author Mahmut Kilic
** @date December, 2020
* \brief This class allows to build new sensors for the robot.
* \brief Other sensors to be built inherit this sensor.
*/

#include "PioneerRobotAPI.h"
#ifndef RANGESENSOR_H_
#define RANGESENSOR_H_
#define MAX_LENGTH 999
class RangeSensor
{
private:
	float ranges[MAX_LENGTH];
	PioneerRobotAPI* robotAPI;

public:
	/*!
	 Returns distance information of the sensor with the index given to the function.
	*/
	virtual float getRange(int)const = 0;
	/*!
	 operator[] returns the sensor value given the index.
	*/
	virtual float operator[](int)const = 0;
	/*!
	 getAngle returns the angle value of the sensor given the index.
	*/
	virtual float getAngle(int)const = 0;
	/*!
	 getMax returns the maximum of the distance values.
	*/
	virtual float getMax(int&)const = 0;
	/*!
	 getMin returns the minimum of the distance values.
	*/
	virtual float getMin(int&)const = 0;
	/*!
	 updateSensor uploads the current sensor distance values of the robot to the ranges array.
	*/
	virtual void updateSensor(float[]) = 0;
	/*!
	 getClosestRange returns the smallest distance between two angles over angle, returns the distance with return.
	*/
	virtual float getClosestRange(float, float, float&)const = 0;
};


#endif 
