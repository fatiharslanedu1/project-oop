/**
 * @file RobotMenu.h
 * @Author Tolga Hardal
 * @date December, 2020
 * @brief Robot Operator functions in class
 */


//#include "PioneerRobotAPI.h"
//#include "Record.h"
//#include "RobotOperator.h"
//#include "Pose.h"
//#include <string.h>
//#include "RobotControl.h"
//#include "LaserSensor.h"
//#include "SonarSensor.h"
//#include "Path.h"
//
//#ifndef ROBOTMENU_H
//#define ROBOTMENU_H
//
//class RobotMenu
//{
//private:
//	RobotControl robot;
//	SonarSensor sonar;
//	LaserSensor laser;
//	Pose updatedPose;
//	Pose tmpPose;
//	Path path;
//	string tmpName;	
//	Record record;
//	
//	int tmpDistance;
//	int choice,a,selected, loop, nloop = 0;
//	float updatedSonar[16], updatedLaser[181];
//	float tmpX, tmpY, tmpTh;
//	float tmpMoveSpeed;
//
//
//public:
//	RobotMenu();
//	~RobotMenu();
//	void menu();
//};
//
//#endif 