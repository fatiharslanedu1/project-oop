﻿/**
 * @file Path.h
 * @Author Oğuzhan Okur
 * @date December, 2020
 * @brief Path class definition.
 */

#include "Node.h"
#include <iostream>
#include <string.h>
using namespace std;
#ifndef PATH_H_
#define PATH_H_

//! Controls operations such as adding and deleting on the list.
class Path {
	Node* head; // head
	Node* tail; // tail
	int number;
public:
	//! Constructor function.
	Path();

	/*!
	add function.
	pose on Pose.
	*/
	void addPos(Pose pose);

	//! print function
	void print();

	/*!
	Get Position for function,
	index refers current index.
	*/
	Pose getPos(int index);
	
	// get head function.
	Node* getHead()const;

	/*!
	remove position function.
	*/
	bool removePos(int index);

	/*!
	insert position,
	pose from Pose.
	*/
	bool insertPos(int index, Pose pose);

	int listCount();

	//! operator funtions.
	Pose operator[](int i);

	//! operator<< function.
	friend ostream &operator<<(ostream &output, const Path &path);

	//! operator>> function.
	friend istream &operator>> (istream &input, Path &path);

};

#endif 
