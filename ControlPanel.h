/**
 * @file ControlPanel.h
 * @Author Fatih Arslan
 * @date January, 2021
 * @brief Control Panel for project.
 */
#include "PioneerRobotAPI.h"
#include "RobotOperator.h"
#include "LaserSensor.h"
#include "Pose.h"
#include "RobotControl.h"
#include "PioneerRobotInterface.h"
#include "Record.h"
#include "SonarSensor.h"
#include "Path.h"
#include <iostream>
#include <string>

#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H
/*!
	control panel class definition.
*/
class ControlPanel
{
	RobotControl robot;
	PioneerRobotInterface pioneerRobot;
	SonarSensor sonar;
	Pose updatedPose;
	Pose tmpPose;
	Path path;
	Record record;
	RobotOperator robotOperator;
	LaserSensor laser;
	string tmpName;
	int tmpPass;
	int tmpDistance;
	int choice, a, selected, loop, nloop = 0;
	float updatedSonar[16], updatedLaser[181];
	float tmpX, tmpY, tmpTh;
	float tmpMoveSpeed;

public:
	//! call Control Panel
	ControlPanel();

	//! call destructor for panel.
	~ControlPanel();

	//! call the user menu.
	void menu();
};

#endif 