/**
* @file SonarSensor.h
* @Author Mahmut Kilic
* @date December, 2020
* \brief This class allows the robot to operate with laser sensors.
*/

#include "PioneerRobotAPI.h"
#include "RangeSensor.h"

#ifndef SONARSENSOR_H_
#define SONARSENSOR_H_

class SonarSensor : public RangeSensor
{
private:
	/*!
	  distance information of the sensor.
	*/
	float ranges[16];
	/*!
	  angle information of the sensor.
	*/
	float angles[16];
	PioneerRobotAPI* robotAPI;
	/*!
	 setAngles sets sensor default values.
	*/
	void setAngles();
public:
	/*!
	 SonarSensor constitutive function of the sensors.
	*/
	SonarSensor();
	/*!
	 LaserSensor constitutive function of the sensors.
	*/
	SonarSensor(float[]);
	float getRange(int)const;
	float operator[](int)const;
	float getAngle(int)const;


	/*!
	  getMax returns the maximum of the distance values.
	*/
	float getMax(int&)const;
	/*!
	 returns the minimum of the distance values.
	*/
	float getMin(int&)const;
	/*!
	  updateSensor  uploads the current sensor distance values of the robot to the ranges array.
	*/
	void updateSensor(float[]);
	/*!
	 getClosestRange returns the nearest distance.
	*/
	float getClosestRange(float, float, float&)const;
};
#endif 

