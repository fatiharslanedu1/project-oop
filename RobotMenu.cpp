//#include "RobotMenu.h"
//
//RobotMenu::RobotMenu()
//{
//}
//
//RobotMenu::~RobotMenu()
//{
//}
//
//void RobotMenu::menu()
//{
//	do {
//		cout << "		  " << endl;
//		cout << "Main Menu" << endl;
//		cout << "1. Connection" << endl;
//		cout << "2. Motion" << endl;
//		cout << "3. Sensor" << endl;
//		cout << "4. Quit" << endl;
//		cout << "Choose one : ";
//		cin >> choice;
//
//		switch (choice)
//		{
//		case 1:  // Connection
//			nloop = 0;
//			do {
//				cout << "				" << endl;
//				cout << "Connection Menu" << endl;
//				cout << "1. Connect Robot" << endl;
//				cout << "2. Disconnect Robot" << endl;
//				cout << "3. Back	" << endl;
//				cout << "Choose one : ";
//				cin >> choice;
//
//				switch (choice)
//				{
//				case 1:  //  Connect Robot
//					cout << "Please enter a name for output file :";
//					cin >> tmpName;
//					record.setFileName(tmpName + ".txt");
//					record.openFile();
//
//					if (!robot.getRobotAPI()->connect())
//					{
//						record.writeLine("Robot could not connect...");
//						cout << "\nRobot could not connect..." << endl;
//					}
//					else
//					{
//						record.writeLine("Robot is connected.");
//						cout << "\nRobot is connected." << endl;
//					}
//					break;
//
//				case 2:  // Disconnect Robot
//					if (!robot.getRobotAPI()->disconnect())
//					{
//						record.writeLine("Robot could not disconnect...");
//						cout << "\nRobot could not disconnect..." << endl;
//					}
//					else
//					{
//						record.writeLine("Robot is disconnected.");
//						cout << "\nRobot is disconnected." << endl;
//					}
//					break;
//
//				case 3:  // Back
//					nloop = 1;
//					break;
//
//				default:
//					// Do nothing
//					break;
//				}
//
//			} while (nloop == 0);
//			break;
//
//		case 2:  // Motion
//			nloop = 0;
//			do {
//				cout << "			" << endl;
//				cout << "Motion Menu" << endl;
//				cout << "1. Move Robot" << endl;
//				cout << "2. Safe Move Robot" << endl;
//				cout << "3. Stop Moving" << endl;
//				cout << "4. Turn Left" << endl;
//				cout << "5. Turn Right" << endl;
//				cout << "6. Forward" << endl;
//				cout << "7. Move Distance" << endl;
//				cout << "8. Close Wall	" << endl;
//				cout << "9. Back" << endl;
//				cout << "Choose one : ";
//				cin >> choice;
//
//				switch (choice)
//				{
//				case 1:  // Move Robot
//					cout << "Please enter the speed of robot : ";
//					cin >> tmpMoveSpeed;
//					robot.getRobotAPI()->moveRobot(tmpMoveSpeed);
//					record.writeLine("Robot is moving with speed of " + to_string(tmpMoveSpeed));
//					break;
//
//				case 2: // Safe Move
//
//					robot.getRobotAPI()->getSonarRange(updatedSonar);
//					sonar.updateSensor(updatedSonar);
//
//					tmpX = tmpPose.getX();
//					tmpY = tmpPose.getY();
//					tmpTh = tmpPose.getTh();
//
//					record.writeLine("Robot's first position (X,Y,Th) : " + to_string(tmpX) + "," + to_string(tmpY) + "," + to_string(tmpTh));
//					
//
//					cout << "Please enter the speed of robot : ";
//					cin >> tmpMoveSpeed;
//
//					record.writeLine("Robot has moved distance of " + to_string(tmpDistance) + " with speed of " + to_string(tmpMoveSpeed));
//
//					while (sonar.getRange(4) > 650 && sonar.getRange(3) > 650) {
//						robot.getRobotAPI()->moveRobot(tmpMoveSpeed);
//						robot.getRobotAPI()->getSonarRange(updatedSonar);
//						sonar.updateSensor(updatedSonar);
//						cout << sonar.getRange(4) << endl;
//					}
//					robot.stopMove();
//					updatedPose.setPose(robot.getRobotAPI()->getX(), robot.getRobotAPI()->getY(), robot.getRobotAPI()->getTh());
//					path.addPos(updatedPose);
//
//					tmpX = updatedPose.getX();
//					tmpY = updatedPose.getY();
//					tmpTh = updatedPose.getTh();
//					record.writeLine("Robot's last position (X,Y,Th) : " + to_string(tmpX) + "," + to_string(tmpY) + "," + to_string(tmpTh));
//
//
//					break;
//				case 3:  // Stop Moving
//					robot.stopMove();
//					record.writeLine("Robot has stopped moving.");
//					break;
//
//				case 4:  // Turn Left
//					robot.turnLeft();
//					record.writeLine("Robot is turning to left.");
//					break;
//
//				case 5:  // Turn Right   
//					robot.turnRight();
//					record.writeLine("Robot is turning to right.");
//					break;
//
//				case 6:  // Forward
//					cout << "Please enter the speed of robot : ";
//					cin >> tmpMoveSpeed;
//					robot.forward(tmpMoveSpeed);
//					record.writeLine("Robot is moving forward with speed of " + to_string(tmpMoveSpeed));
//					break;
//
//				case 7:  // Move Distance
//					updatedPose.setPose(robot.getRobotAPI()->getX(), robot.getRobotAPI()->getY(), robot.getRobotAPI()->getTh());
//					path.addPos(updatedPose);
//					tmpPose = updatedPose;
//
//					cout << "Please enter distance to go : ";
//					cin >> tmpDistance;
//
//					cout << "Please enter the speed of robot : ";
//					cin >> tmpMoveSpeed;
//
//					while (updatedPose.findDistanceTo(tmpPose)<=tmpDistance)
//					{
//						robot.getRobotAPI()->moveRobot(tmpMoveSpeed);
//						updatedPose.setPose(robot.getRobotAPI()->getX(), robot.getRobotAPI()->getY(), robot.getRobotAPI()->getTh());
//						path.addPos(updatedPose);
//					}
//					robot.stopMove();
//
//					tmpX = tmpPose.getX();
//					tmpY = tmpPose.getY();
//					tmpTh = tmpPose.getTh();
//
//					record.writeLine("Robot's first position (X,Y,Th) : " + to_string(tmpX) + "," + to_string(tmpY) + "," + to_string(tmpTh));
//					record.writeLine("Robot has moved distance of " + to_string(tmpDistance) + " with speed of " + to_string(tmpMoveSpeed));
//
//					tmpX = updatedPose.getX();
//					tmpY = updatedPose.getY();
//					tmpTh = updatedPose.getTh();
//					record.writeLine("Robot's last position (X,Y,Th) : " + to_string(tmpX) + "," + to_string(tmpY) + "," + to_string(tmpTh));
//					break;
//
//				case 8:  // Close Wall
//
//					break;
//
//				case 9:  // Back
//					nloop = 1;
//					break;
//
//				default:
//					// Do nothing
//					break;
//				}
//			} while (nloop == 0);
//			break;
//
//		case 3:  // Sensor  
//
//			nloop = 0;
//			do {
//				cout << "	" << endl;
//				cout << "1. Get range from a selected laser sensor" << endl;
//				cout << "2. Get minimum range from laser sensor	" << endl;
//				cout << "3. Get maximum range from laser sensor	" << endl;
//				cout << "4. Get angle of a selected laser sensor	" << endl;
//				cout << "5. Get range from a selected sonar sensor" << endl;
//				cout << "6. Get minimum range from sonar sensor	" << endl;
//				cout << "7. Get maximum range from sonar sensor	" << endl;
//				cout << "8. Get angle of a selected sonar sensor	" << endl;
//				cout << "9. Update laser sensor" << endl;
//				cout << "10. Update sonar sensor" << endl;
//				cout << "11. Back" << endl;
//				cout << "Choose one : ";
//				cin >> choice;
//
//				switch (choice)
//				{
//				case 1:
//					cout << "Enter a integer[0-180] to find range of laser sensor -> ";
//					cin >> selected;
//					if ((selected >=0) && (selected <= 180))
//					{
//						cout << "-> " << laser.getRange(selected) << endl;
//					}
//					break;
//				case 2: 
//					laser.getMin(a);
//					cout << "Minimum range of laser sensor " << a << "range " << laser.getMin(a) <<  endl;
//					break;
//				case 3:
//					laser.getMax(a);
//					cout << "Maximum range of laser sensor " << a << "range " << laser.getMax(a) << endl;
//					break;
//				case 4:
//					cout << "Enter a integer[0-180] to find angle of laser sensor -> ";
//					cin >> selected;
//					if ((selected >= 0) && (selected <= 180))
//					{
//						cout << "-> " << laser.getAngle(selected) << endl;
//					}
//					break;
//				case 5:
//					cout << "Enter a integer[0-15] to find range of sonar sensor -> ";
//					cin >> selected;
//					if ((selected >= 0) && (selected <= 15))
//					{
//						cout << "-> " << sonar.getRange(selected) << endl;
//					}
//					break;
//				case 6:
//					sonar.getMin(a);
//					cout << "Minimum range of sonar sensor " << a << "range " << sonar.getMin(a) << endl;
//					break;
//				case 7:
//					int a;
//					sonar.getMax(a);
//					cout << "Maximum range of sonar sensor " << a << "range " << sonar.getMax(a) << endl;
//					break;
//				case 8:
//					cout << "Enter a integer[0-15] to find angle of sonar sensor -> ";
//					cin >> selected;
//					if ((selected >= 0) && (selected <= 15))
//					{
//						cout << "-> " << sonar.getAngle(selected) << endl;
//					}
//					break;
//				case 9:
//					robot.getRobotAPI()->getLaserRange(updatedLaser);
//					laser.updateSensor(updatedLaser);
//
//					record.writeLine("Laser sensor updated");
//					break;
//				case 10:
//					robot.getRobotAPI()->getSonarRange(updatedSonar);
//					sonar.updateSensor(updatedSonar);
//
//					record.writeLine("Sonar sensor updated");
//					break;
//				case 11:
//					nloop = 1;
//					break;
//				default:
//					break;
//				}
//
//
//			} while (nloop == 0);
//			break;
//
//		case 4:  // Quit
//			loop = 1;
//			break;
//
//		default:
//			// Do nothing
//			break;
//		}
//	} while (loop == 0);
//}