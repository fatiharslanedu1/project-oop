/**
 * @file Record.h
 * @Author Fatih Arslan
 * @date December, 2020
 * @brief Record robot movements, and rating on output file.
 */
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#ifndef RECORD_H
#define RECORD_H

class Record
{
	string fileName;
	fstream file;
public:
	//! function which is open the file.
	bool openFile();

	//! function which is close the file.
	bool closeFile();

	//! function which is set file name.
	void setFileName(string name);

	//! function which is read line by line.
	string readLine();

	//! function for writes lines.
	bool writeLine(string str);

	//! overload << for output.
	friend ostream &operator<<(ostream&, const Record&);

	//! operator overload >> for input
	friend istream &operator >> (istream&, Record&);
};

#endif