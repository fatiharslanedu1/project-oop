/**
* @file Encryption.h
* @Author Tolga Hardal
* @date December, 2020
* \brief A Class to Encrypt and Decrypt given values.
*/

#ifndef ENCRYPTION_H
#define ENCRYPTION_H


class Encryption
{
public:
	/*!
	encrypt given int
    */
	int encrypt(int);
	/*!
	decrypt given int
	*/
	int decrypt(int);
};

#endif
