//** RobotControl.cpp
#include<iostream>
#include"RobotControl.h"

using namespace std;

//! Get to object(robotAPI)
//! Constructor Functions
RobotControl::RobotControl(RobotInterface* robo, RangeSensor* sen1, RangeSensor* sen2, RobotOperator* robotOpo)
{
	robotInterface = robo;
	sensor1 = sen1;
	sensor2 = sen2;
	robotOperator = robotOpo;
}
//! Turn left Function 
void RobotControl::turnLeft() {

	robotInterface->turnLeft();

}
//! Turn right Function
void RobotControl::turnRight() {

	robotInterface->turnRight();

}

//! Forward Function
/*!
speed to contunie with given speed.
*/
void RobotControl::forward(float speed) {

	robotInterface->forward(speed);

}
//! Print to position of robot.
void RobotControl::print() {
	Pose pose = robotInterface->getPose();
	cout << "MyPose is (" << pose.getX() << "," << pose.getY() << "," << pose.getTh() << ")" << endl;
}

//! speed (to go back with given speed)
void RobotControl::backward(float speed) {

	robotInterface->backward(speed);
}

/*!
position from Pose class,
Get function of pose
*/
Pose RobotControl::getPose() {

	return robotInterface->getPose();
}

/*!
p taken from Pose class,
Set function of pose to PioneerRobotAPI from Pose.
*/
void RobotControl::setPose(Pose p) {
	robotInterface->setPose(p);
}

//! function to stop turning.
void RobotControl::stopTurn() {
	robotInterface->stopTurn();
}

//! function to stop moving.
void RobotControl::stopMove() {
	robotInterface->stopMove();
}

//! Destructor function.
RobotControl::~RobotControl(){
};

//! Access Modifiers implementation.
//! function for open Access.
bool RobotControl::openAccess(int a)
{
	if (robotOperator->checkAccessCode(a))
	{
		robotOperator->setAccessState(true);
		return true;
	}
	else
	{
		return false;
	}
}
//! function for close Access.
bool RobotControl::closeAccess(int a)
{
	if (robotOperator->checkAccessCode(a))
	{
		robotOperator->setAccessState(false);
		return true;
	}
	else
	{
		return false;
	}
}

//! function for record operation which is through to file.
bool RobotControl::recordPathToFile() {
	Pose pose;
	for (int i = 1; i <= path.listCount(); i++) {
		pose = path.getPos(i);
		record.writeLine("Robot's position (X,Y,Th) : " + to_string(pose.getX()) + "," + to_string(pose.getY()) + "," + to_string(pose.getTh()));
	}
	return true;
}

// Edit by Mahmut...
//! function for adding path.
bool RobotControl::addToPath() {
	path.addPos(robotInterface->getPose());
	return true;
}

//! function for clear path.
bool RobotControl::clearPath() {
	while (path.removePos(1));
	return true;
}

