//!! PioneerRobotInterface.cpp


#include<iostream>
#include"PioneerRobotInterface.h"

using namespace std;

//! Constructor functions.
PioneerRobotInterface::PioneerRobotInterface()
{

	robotAPI = new PioneerRobotAPI();
	
}
//! turn left functions.
void PioneerRobotInterface::turnLeft() {

	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);

}
//! turn right functions.
void PioneerRobotInterface::turnRight() {

	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);

}
//! forward functions.
void PioneerRobotInterface::forward(float speed) {

	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
	robotAPI->moveRobot(speed);

}
//! position of robot.
void PioneerRobotInterface::print() {
	cout << "MyPose is (" << position->getX() << "," << position->getY() << "," << position->getTh() << ")" << endl;
}
//! speed to go back with given speed.
void PioneerRobotInterface::backward(float speed) {

	float th1;
	Pose p, tempPose;
	tempPose.setPose(robotAPI->getX(), robotAPI->getY(), robotAPI->getTh());
	p.setTh(tempPose.getTh());
	th1 = tempPose.findAngleTo(p);
	th1 += 180;
	tempPose.setTh(robotAPI->getTh());
	robotAPI->setPose(tempPose.getX(), tempPose.getY(), tempPose.getTh());
	robotAPI->moveRobot(speed);

}
//! Get function of pose.
Pose PioneerRobotInterface::getPose() {
	Pose tempPose;
	tempPose.setPose(robotAPI->getX(), robotAPI->getY(), robotAPI->getTh());
	return tempPose;
}
//! Set function of pose to PioneerRobotAPI from Pose.
void PioneerRobotInterface::setPose(Pose p) {
	float X1, Y1, TH1;
	X1 = p.getX();
	Y1 = p.getY();
	TH1 = p.getTh();
	robotAPI->setPose(X1, Y1, TH1);
}
//! function to stop turning.
void PioneerRobotInterface::stopTurn() {
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
}
//! function to stop turning.
void PioneerRobotInterface::stopMove() {
	robotAPI->stopRobot();
}


PioneerRobotInterface::~PioneerRobotInterface() {
	delete robotAPI;
	delete position;
}
// edit by Tolga
bool PioneerRobotInterface::connect() {
	return robotAPI->connect();
}

bool PioneerRobotInterface::disconnect() {
	return robotAPI->disconnect();
}
void PioneerRobotInterface::getSonarRanges(float ranges[]) {
	robotAPI->getSonarRange(ranges);
}

void PioneerRobotInterface::getLaserRanges(float ranges[]) {
	robotAPI->getLaserRange(ranges);
}



void PioneerRobotInterface::updateSensors()
{

}
